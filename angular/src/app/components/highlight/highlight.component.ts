import { MoviesService } from './../../services/movies.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.css']
})
export class HighlightComponent implements OnInit {

  constructor(public movieService: MoviesService) { }

  ngOnInit(): void {
  }

  public highlightPic(): string{
    const highlightPicPath = "../../../assets/img/";
    return highlightPicPath + this.movieService.getSelectedMovie().highlightPic;
  }

}
