import { MoviesService } from './../../services/movies.service';
import { Component, OnInit } from '@angular/core';
import Movie from 'src/app/models/movie-model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  public movieList!: Movie[];

  constructor(private movieService: MoviesService) { }

  ngOnInit(): void {
    this.movieList = this.movieService.getMovies();
  }

}
