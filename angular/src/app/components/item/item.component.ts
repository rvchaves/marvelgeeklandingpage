import { MoviesService } from './../../services/movies.service';
import Movie from 'src/app/models/movie-model';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() item!: Movie;
  constructor(private movieService: MoviesService) { }

  ngOnInit(): void {
  }

  selectMovie(item: Movie): void{
    this.movieService.setSelectedMovie(item);
  }

}
