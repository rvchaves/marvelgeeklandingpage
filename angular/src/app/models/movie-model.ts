export default class Movie {

  constructor(Id: number, Title: string, ReleaseYear: number, Synopsis: string, Pic: string, HighlightPic: string) {
    this.id = Id;
    this.title = Title;
    this.releaseYear = ReleaseYear;
    this.synopsis = Synopsis;
    this.pic = Pic;
    this.highlightPic = HighlightPic;
  }

  public id: number;
  public title: string;
  public releaseYear: number
  public synopsis: string;
  public pic: string;
  public highlightPic: string;
}
